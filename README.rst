Ruft die ADFC api-touren-termine auf und erstellt daraus eine
ics-Kalenderdatei, die in den eigenen Kalender eingebunden werden kann, um so
alle Termine vom ADFC-Dresden auf dem Computer zu sehen. Diese generieren wir
täglich und laden den via ftp auf Diesen laden wir dann wie ftp auf unseren
Server.

Um diesen in deinem Kalender-Programm zu verwenden, erstelle dort einen neuen
Webkalender mit der URL https://ics.adfc-dresden.de/adfc-dresden-termine.ics.

Falls du für deine ADFC-Gruppe, ähnliches anbieten willst, unterstützen wir dich gerne.
