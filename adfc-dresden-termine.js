var jsdom = require("jsdom");
var $ = require('jquery/dist/jquery')(new jsdom.JSDOM().window);

var beginning_param = '&beginning=' + new Date().toISOString().slice(0,10);
var api_url_with_params = 'https://api-touren-termine.adfc.de//api/eventItems/search?unitKey=142002&sort=date&cancelled=false' + beginning_param;

function to_ics_lines(data)
{
    var lines = [];
    var icsDatetimeFormat = 'YYYYMMDDTHHmmSS';
    
    lines.push("BEGIN:VCALENDAR");
    lines.push("VERSION:2.0");
    lines.push("PRODID:https://www.adfc-dresden.de/");
    lines.push("METHOD:PUBLISH");
    
    for (var i=0; i < data.items.length; i++)
    {
        var item = data.items[i];
        
        lines.push("BEGIN:VEVENT");
        lines.push("UID:"         + item.eventItemId);
        lines.push("SUMMARY:"     + item.title);
        lines.push("DESCRIPTION:" + item.cShortDescription);
        lines.push("LOCATION:"    + item.city);
        lines.push("GEO:"         + item.latitude + ";" + item.longitude);
        lines.push("DTSTART:"     + new Date(item.beginning).toISOString().replace(new RegExp('[-:]', 'g'), '').slice(0,15) + "Z");
        lines.push("DTEND:"       + new Date(item.end      ).toISOString().replace(new RegExp('[-:]', 'g'), '').slice(0,15) + "Z");
        lines.push("DTSTAMP:"     + new Date(              ).toISOString().replace(new RegExp('[-:]', 'g'), '').slice(0,15) + "Z");
        lines.push("CLASS:PUBLIC");
        lines.push("URL:https://touren-termine.adfc.de/radveranstaltung/" + item.cSlug);
        lines.push("END:VEVENT");
    }
    
    lines.push("END:VCALENDAR");
    return lines;
}

$.ajax({
    dataType: 'json',
    url: api_url_with_params,
}).then(data => to_ics_lines(data)).then(lines => console.log(lines.join('\n')));
